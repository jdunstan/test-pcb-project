#### To run this file, from windows explorer, right-click > "Run with PowerShell" ###
echo @off

# Variables for Altium DevOps
$altiumDevOpsPackageVersion = "develop" # Altium DevOps Package version, e.g., "v0.0.4". Check Altium DevOps Releases...
$projectFolder = "PCBA2" # path to folder containing PrjPcb e.g., "PCBA". Use "." if project at the root of the directory
$scriptCmdOptions = "output,drc,compile" # Jobs seperated by commas with no spaces, e.g. "RunDRC,RunCompile,RunOutputJobs"

Invoke-WebRequest -OutFile self_import.ps1 -Headers @{"PRIVATE-TOKEN"="KGt7sam1Uq1Go3aDiXdU"} -Uri "https://gitlab.com/api/v4/projects/28175984/repository/files/self_import.ps1/raw?ref=$altiumDevOpsPackageVersion"
./self_import.ps1

exit $LastExitCode
